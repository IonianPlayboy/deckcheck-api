import util from "util";
import { writeFile, mkdir, readdir } from "fs";
import fetch from "node-fetch";

const promWriteFile = util.promisify(writeFile);
const promMkdir = util.promisify(mkdir);
const promReaddir = util.promisify(readdir);

const fetchScryfallData = async (): Promise<void> => {
	await promMkdir("./data", { recursive: true }).catch(err => {
		throw err;
	});
	const idToName: { [x: string]: string } = {};
	let res;
	let currentCards = 0;
	console.log("start");
	res = await fetch(
		"https://api.scryfall.com/cards/search?unique=cards&q=f%3Ahistoric"
	)
		.then(res => res.json())
		.catch(err => {
			throw err;
		});
	while (res) {
		for (const card of res.data) {
			idToName[card.name] = card.tcgplayer_id;
			if (card.name.includes("//")) {
				const splitedName = card.name.split(" // ");
				splitedName.forEach(
					(el: string) => (idToName[el] = card.tcgplayer_id)
				);
			}
			await promWriteFile(
				`data/${card.tcgplayer_id}.json`,
				JSON.stringify(card)
			).catch(err => {
				throw err;
			});
		}
		currentCards += res.data.length;
		console.log(currentCards);
		res =
			res && res.has_more
				? await fetch(res.next_page)
						.then(res => res.json())
						.catch(err => {
							throw err;
						})
				: null;
	}
	promWriteFile("idToName.json", JSON.stringify(idToName)).catch(err => {
		throw err;
	});
	const dir = "./data";
	promReaddir(dir)
		.then(files =>
			console.log(
				`Number of Cards successfully stocked : ${files.length}`
			)
		)
		.catch(err => {
			throw err;
		});
};

fetchScryfallData().catch(err => {
	throw err;
});
