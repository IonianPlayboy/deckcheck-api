import { Handler, Context, APIGatewayEvent } from "aws-lambda";
import idToName from "../idToName.json";

interface Response {
	statusCode: number;
	headers: {
		[x: string]: string;
	};
	body: string;
}

interface Card {
	name: string;
	// eslint-disable-next-line camelcase
	tcgplayer_id: number;
}
type CardName = keyof typeof idToName;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const handler: Handler = async (event: APIGatewayEvent, _context: Context) => {
	try {
		console.log(event.queryStringParameters);
		const queryParam = event.queryStringParameters
			? event.queryStringParameters.q
			: null;
		const pathParam = event.path.replace("/search/", "");
		const requestedCard = ((queryParam
			? queryParam
			: pathParam) as unknown) as CardName;
		if (idToName[requestedCard]) {
			// eslint-disable-next-line @typescript-eslint/no-var-requires
			const card = require(`../data/${idToName[requestedCard]}.json`) as Card;
			const response: Response = {
				statusCode: 200,
				headers: {
					"Content-Type": "application/json; charset=utf-8",
					"Access-Control-Allow-Origin": "*"
				},
				body: JSON.stringify(card)
			};
			return response;
		} else {
			const response: Response = {
				statusCode: 404,
				headers: {
					"Content-Type": "application/json",
					"Access-Control-Allow-Origin": "*"
				},
				body: JSON.stringify({
					object: "error",
					code: "not_found",
					status: 404,
					details:
						"Could not find the card you were looking for. Sorry. :c"
				})
			};
			return response;
		}
	} catch (err) {
		return {
			statusCode: 500,
			headers: {
				"Content-Type": "application/json; charset=utf-8",
				"Access-Control-Allow-Origin": "*"
			},
			body: err.toString()
		} as Response;
	}
};

export { handler };
